import { IsNotEmpty, IsNumberString, IsOptional } from "class-validator";

export class CreateMessageDto {

    @IsNotEmpty()
    sender_name: string;

    @IsNotEmpty()
    post: string;

    @IsOptional()
    @IsNumberString()
    parent_message_id: number;
}
